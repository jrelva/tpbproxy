# The Pirate Bay #
## Load-balanced Proxy ##

A preconfigured load-balanced and redundant nginx proxy for The Pirate Bay.

## Usage ##

```
docker pull jrelva/tpbproxy
docker run -d -p 80:80 -t jrelva/tpbproxy

```

Now visit your IP address, it's a simple as that.
