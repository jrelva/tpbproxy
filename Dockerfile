FROM nginx

ADD nginx.conf /etc/nginx.conf
ADD index.html /usr/local/nginx/html/index.html

EXPOSE 80

